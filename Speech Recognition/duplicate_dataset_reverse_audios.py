# Convert all the files into images:
#Extracts filter bank for every image.
#Dependencies:
#pip install tqdm
import sys
import os
import soundfile as sf
sys.path.append("..") # Adds higher directory to python modules path. This is important to load the libraries  in PROJECT_HOME/src/
import src.d01_load_data as ld #Library to load data and handle data.

#PROJECT_HOME = os.getcwd()+"/"
# NOTE that in this Jupyter notebook, the parent folder is the project home, not this!
PROJECT_HOME = os.path.dirname(os.getcwd())+"/"

SR_DATA="Speech Recognition/speech_commands_v0.01/" #Command dataset folder.
#SR_DATA="Speech Recognition/speech_commands_mini/" #Command dataset folder.

from PIL import Image
import os
import shutil
from tqdm import tqdm
import numpy as np
#Firstly, list all the folders (categories)
dataset_folders = next(os.walk(PROJECT_HOME+SR_DATA))[1]

#As the background noise is not a command, take it out of the pipeline.
not_a_command = "_background_noise_"
if(not_a_command in dataset_folders):
    dataset_folders.remove(not_a_command)

print(dataset_folders)
#Create a folder in the current directory to store the transformed Filter banks
if(SR_DATA[-1] == "/"):
    SR_DATA_IMG = SR_DATA[:-1]+'_duplicate/' #Delete / , otherwise it will be interpreted as a child directory, instead of creating a sibiling dir.
else:
    SR_DATA_IMG = SR_DATA+'_duplicate/'

#If it fails, the directory already exists.
#
#Check if it exists
if(os.path.isdir(PROJECT_HOME+SR_DATA_IMG)):
    #Delete all the folder content
    print("Folder "+SR_DATA_IMG+" already exists, overwriting...")
    shutil.rmtree(PROJECT_HOME+SR_DATA_IMG)
    
os.mkdir(PROJECT_HOME+SR_DATA_IMG)


print("Created directory "+SR_DATA_IMG)

for category_dir in dataset_folders:
    #Go one by one generating the filter banks and storing them in the new folder as images.
    print("Augmenting sound files for category ["+category_dir+"]")
    fullCatDir = PROJECT_HOME+SR_DATA+category_dir
    #Create folder for category in IMG:
    fullCatDirImg = PROJECT_HOME+SR_DATA_IMG+category_dir
    if(category_dir=='on' or category_dir=='no' or category_dir=='wow'):
        fullCatDirImgRev = PROJECT_HOME+SR_DATA_IMG+category_dir[::-1]+'_r' #Inverse name
    else:
        fullCatDirImgRev = PROJECT_HOME+SR_DATA_IMG+category_dir[::-1] #Inverse name
    os.mkdir(fullCatDirImg)
    os.mkdir(fullCatDirImgRev)

    
    list_files = next(os.walk(fullCatDir))[2]
    tmpLabels = []

    for i in tqdm(range(0, len(list_files))):
        fAudio = ld.Audio( fullCatDir+"/"+list_files[i] )
        baseName = list_files[i] [:list_files[i].index('.')] # Name without extension i.e. dog01.wav -> dog
        
        samp, sr = fAudio.read()
        samp_inverse = samp[::-1]
        sf.write(fullCatDirImg+'/'+baseName+'.wav', samp, sr)
        sf.write(fullCatDirImgRev+'/'+baseName+'.wav', samp_inverse, sr)
        
        

print("Job completed :)")
print("")