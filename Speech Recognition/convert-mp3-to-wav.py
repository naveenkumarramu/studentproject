## Convert all the files into images:
#Extracts filter bank for every image.
#Dependencies:
#pip install tqdm
import sys
import os

sys.path.append("..") # Adds higher directory to python modules path. This is important to load the libraries  in PROJECT_HOME/src/

# NOTE that in this Jupyter notebook, the parent folder is the project home, not this!
PROJECT_HOME = os.path.dirname(os.getcwd())+"/"

#SR_DATA="Speech Recognition/commands_multilanguage/german/" #Command dataset folder.
SR_DATA="Speech Recognition/commands_multilanguage/spanish/" #Command dataset folder.

import os
import shutil
from tqdm import tqdm
#Firstly, list all the folders (categories)
dataset_folders = next(os.walk(PROJECT_HOME+SR_DATA))[1]
#Create a folder in the current directory to store the transformed Filter banks
if(SR_DATA[-1] == "/"):
    SR_DATA_IMG = SR_DATA[:-1]+'_WAV/' #Delete / , otherwise it will be interpreted as a child directory, instead of creating a sibiling dir.
else:
    SR_DATA_IMG = SR_DATA+'_WAV/'

#If it fails, the directory already exists.
#
#Check if it exists
if(os.path.isdir(PROJECT_HOME+SR_DATA_IMG)):
    #Delete all the folder content
    print("Folder "+SR_DATA_IMG+" already exists, overriding...")
    shutil.rmtree(PROJECT_HOME+SR_DATA_IMG)
    
os.mkdir(PROJECT_HOME+SR_DATA_IMG)


print("Created directory "+SR_DATA_IMG)
command = "ffmpeg -i "
for category_dir in dataset_folders:
    #Go one by one generating the filter banks and storing them in the new folder as images.
    print("Converting audios for category ["+category_dir+"]")
    #os.system('ls')

    fullCatDir = PROJECT_HOME+SR_DATA+category_dir
    #Create folder for category in IMG:
    fullCatDirImg = PROJECT_HOME+SR_DATA_IMG+category_dir
    os.mkdir(fullCatDirImg)


    list_files = next(os.walk(fullCatDir))[2]
    tmpLabels = []

    for i in tqdm(range(0, len(list_files))):
        #print("File: "+fullCatDir+"/"+list_files[i])
        print("Converting: "+list_files[i])
        newName = list_files[i][:-3]+"wav"
        # Concatenates "ffmpeg -i inputFile.mp3 -ss 0 -to 1 outputFile.wav"
        #-ss 0 means from start of audio; -to 1 means up to one second.
        os.system(command+"'"+fullCatDir+"/"+list_files[i]+"'"+" -ss 0 -to 1 "+"'"+fullCatDirImg+"/"+newName+"'")


