## Convert all the files into images:
#Extracts filter bank for every image.
#Dependencies:
#pip install tqdm
import sys
import os

sys.path.append("..") # Adds higher directory to python modules path. This is important to load the libraries  in PROJECT_HOME/src/
import src.d01_load_data as ld #Library to load data and handle data.

#PROJECT_HOME = os.getcwd()+"/"
# NOTE that in this Jupyter notebook, the parent folder is the project home, not this!
PROJECT_HOME = os.path.dirname(os.getcwd())+"/"

SR_DATA="Speaker Identification/data/" #Command dataset folder.
#SR_DATA="Speech Recognition/speech_commands_mini/" #Command dataset folder.

from PIL import Image
import os
import shutil
from tqdm import tqdm
import numpy as np
#Firstly, list all the folders (categories)
dataset_folders = next(os.walk(PROJECT_HOME+SR_DATA))[1]

#As the background noise is not a command, take it out of the pipeline.
not_a_command = "_background_noise_"
if(not_a_command in dataset_folders):
    dataset_folders.remove(not_a_command)

print(dataset_folders)
#Create a folder in the current directory to store the transformed Filter banks
if(SR_DATA[-1] == "/"):
    SR_DATA_IMG = SR_DATA[:-1]+'_IMG/' #Delete / , otherwise it will be interpreted as a child directory, instead of creating a sibiling dir.
else:
    SR_DATA_IMG = SR_DATA+'_IMG/'

#If it fails, the directory already exists.
#
#Check if it exists
if(os.path.isdir(PROJECT_HOME+SR_DATA_IMG)):
    #Delete all the folder content
    print("Folder "+SR_DATA_IMG+" already exists, overriding...")
    shutil.rmtree(PROJECT_HOME+SR_DATA_IMG)
    
os.mkdir(PROJECT_HOME+SR_DATA_IMG)


print("Created directory "+SR_DATA_IMG)

for category_dir in dataset_folders:
    #Go one by one generating the filter banks and storing them in the new folder as images.
    print("Extracting Filter Banks for category ["+category_dir+"]")
    fullCatDir = PROJECT_HOME+SR_DATA+category_dir
    #Create folder for category in IMG:
    fullCatDirImg = PROJECT_HOME+SR_DATA_IMG+category_dir
    os.mkdir(fullCatDirImg)


    list_files = next(os.walk(fullCatDir))[2]
    tmpLabels = []

    for i in tqdm(range(0, len(list_files))):
        fAudio = ld.Audio( fullCatDir+"/"+list_files[i] )
        baseName = list_files[i] [:list_files[i].index('.')] # Name without extension i.e. dog01.wav -> dog

        samp, sr = fAudio.read()

        sFilBank_list = fAudio.transformToFilterBank(N=1.0,trimSilence=True)
        for j in range(len(sFilBank_list)):
            sImage = sFilBank_list[j]
            sImage = sImage.T #Important! The image should have as first axis the filters and as second the features itself.
            sImage = sImage.astype(np.uint8)
            #Improve conversion to PNG. Source: https://stackoverflow.com/questions/56719138/how-can-i-save-a-librosa-spectrogram-plot-as-a-specific-sized-image/57204349#57204349
            sImage = np.flip(sImage, axis=0) # put low frequencies at the bottom in image
            sImage = 255-sImage # invert. make black==more energy

            im = Image.fromarray(sImage)
            if(j==0):
                #print(fullCatDirImg+"/"+baseName+".jpeg")
                im.save(fullCatDirImg+"/"+baseName+".png",'PNG',optimize=False,compress_level=0)
            else:
                #By multiple images, use the number to identify it:
                im.save(fullCatDirImg+"/"+baseName+"_"+str(j)+".png",'PNG',optimize=False,compress_level=0)


print("process completed :) now get your coffee")
## Convert google command dataset to Speaker Dataset:
#### Necessary to have the filter banks as images!