The code from: https://github.com/grausof/keras-sincnet

Steps:
1. Dataset should be downloaded from google colab
2. OUTPUT_FOLDER should be created by TIMIT_preparation.py. It's used as input to the model
3. cfg/SincNet_TIMIT.cfg file should be updated based on the path on the local machine. Check " Run the speaker id experiment." section on the website.
4. RUN python train.py --cfg=cfg/SincNet_TIMIT.cfg command, training should be started. 

Ps: Reorganizing.py is for reorganizing the dataset based on the author's TIMIT_all.scp file. If google colab does not work, it can be applied to original TIMIT dataset. (Rafael's version)
