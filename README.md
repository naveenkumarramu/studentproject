# Model-Agnostic Meta-Learning in the Context of Speech Algorithms

## Getting Started



### Prerequisites

Some important libraries to install:
librosa
```
conda install -c conda-forge librosa
pip install librosa
```
In case of troubles with librosa:
```
conda install -c numba numba
conda install -c conda-forge librosa
```

Python speech features
```
pip install python_speech_features
pip install SpeechRecognition
```
Sounfile
```
pip install SoundFile
```

FFMPEG

To read FLAC files, it is used the backend FFMPEG, this is commonly already available for Linux and Mac systems, for Windows download the zip, extract it in the disk and add it to the PATH:
https://www.ffmpeg.org/download.html
http://blog.gregzaal.com/how-to-install-ffmpeg-on-windows/

Hidden Markov Models
```
pip install --upgrade --user hmmlearn
```


### Project structure
```
├── README.md          <- The top-level README for developers.
├── Model_Agnostic_Meta_Learning.ipynb <- The best structured outcome of the project goes here, 
│                                         always make use of the functions from src to keep this 
│                                         file as concrete as posible.
├── data
│
├── docs               <- Space of documentation
│
├── notebooks          <- Jupyter notebooks to make general experiments of the algorithms.
│
├── references         <- Papers or other ressources used for the project.
│
├── results            <- Final analysis docs or the reference to our Drive Documents.
│
├── .gitignore         <- Avoids uploading data, credentials, 
|                         outputs, system files etc
│
└── src                <- Source code for use in this project.
    ├── __init__.py      <- Makes src a Python module
    │
    ├── d01_load_data.py <- This class loads the files and process them for later use of the classifiers.
    │
    ├── d02_models.py    <- In this section are included the specific models used in our project.
    │
    ├── d03_visualization<- In this section are included the classes and functions for visualization of our data.
    │
    └── d04_utils.py     <- In this section are included the extra utils used in the project.


```
### Installing

## Authors

* **Ece Atalay**
* **Naveen kumar Ramu**
* **Santiago Tena Hernandez**
* **Josué Daniel Rodriguez Quintana**


## Supervisors
* **Rafael Rêgo Drumond, M.Sc.**
* **Prof. Dr. Dr. Lars Schmidt-Thieme**

## License



## Acknowledgments

* Reference furhter authors.

