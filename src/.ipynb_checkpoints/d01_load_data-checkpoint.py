# Model Agnostic Meta Learning
# This class loads the files and process them for later use of the classifiers.

import soundfile as sf 
import numpy as np
import IPython.display as ipd
import matplotlib.pyplot as plt
import librosa
import librosa.display
import logging as log
import os
import random

class DataLoader(object):
    def __init__(self, PROJECT_HOME,DATA_FOLDER):
        self.PROJECT_HOME = PROJECT_HOME
        self.DATA_FOLDER = DATA_FOLDER
        self.samplerate = None
        self.categories = []
        self.train_data = []
        self.train_label = []
        self.test_data = []
        self.test_label = []
        self.val_data = []
        self.val_label = []         
        
    def getRandomFile():
        return ""
    
    def readFile(self,fileName):
        self.data, self.samplerate = AudioFile(self.PROJECT_HOME+self.DATA_FOLDER+fileName)
        return self.data,self.samplerate
    
    def info(self):
        print("Project HOME_PATH: " + self.PROJECT_HOME + ".")
        
    def setCategories(self,cat):
        """Establishes the categories of the dataset in an array, it must fit with the folders"""
        self.categories = cat
    def resetDataFromLoader(self):
        """Resets all the data from the Data Loader for Training, Testing and validation"""
        self.train_data = []
        self.train_label = []
        self.test_data = []
        self.test_label = []
        self.val_data = []
        self.val_label = [] 
        
    def readFilesFromCategory(self,cat,label):
        """Reads all the files from this category"""
        fname = self.PROJECT_HOME+self.DATA_FOLDER+cat+"/"
        files = [v for v in os.listdir(fname) if v[0] != "."]
        
        #split = len(files) - int(len(files)*0.30)
        nFiles = len(files)
        audio_data = np.array([])
        labels = []        
        #Dividing the audio file into 1 second audio chunks for training
        for file in range(0, nFiles):
            fAudio = Audio(fname+files[file])
            fAudio.setCategory(cat)#This category
            fAudio.read()
            splitAudios = fAudio.splitAudio(5,1) # Min duration, deltaT  What do they mean?
            #Stack features.
            if audio_data.size!=0:
                audio_data = np.vstack((audio_data,splitAudios))
            else:
                audio_data = splitAudios
                
        labels = [label] * audio_data.shape[0] ##Creates a list of size N with the same label.
        #labels = np.full((1, audio_data.shape[0]), label)

        return audio_data,labels
    
    def splitTrainingTest(self,pData,labels,splitSizeTest=0.30,shuffle=True):
        """This function splits the data and its labels into training and test data.
            pData = Processed data
            splitTest= Percentage of data for testing."""
        if(shuffle):
            #To shuffle "smartly" without losing the labels, generate a list of the size of the data, shuffle it and take the values as index from the data.
            numData = pData.shape[0] # number of data points.
            indx = list(range(0, numData))
            random.shuffle(indx)
            pDataCopy = np.array(pData, copy=True)
            labelsCopy = np.array(labels, copy=True)
            i=0 # this is the sequential index, while ix will be the index from the list.
            for ix in indx:
                pDataCopy[i] = pData[ix]
                labelsCopy[i] = labels[ix]      
                
            pData=pDataCopy
            labels=labelsCopy
        ### Split data.
        iSplit = len(labels) - int(len(labels)*splitSizeTest)
        
        dTrain = pData[:iSplit,:]
        dTest = pData[iSplit+1:,:]
        
        lTrain = labels[:iSplit]
        lTest = labels[iSplit+1:]
        
        return dTrain,lTrain, dTest, lTest
        
    def processDatasetForHMM(self):
        """Reads the audios and creates the training and testing sets.
            For HMM we make the distinction per class, as we are training each model only with the specific data to later on select from each model the most likely.
        """
        
        #cat is a generic name for speaker, command etc. according to the type of dataset.
        N = 0
        self.resetDataFromLoader() #In case there is data, clean it before using it.
        
        for cat in self.categories:
            
            data, label = self.readFilesFromCategory(cat,N) #N stands for the numerical label.
            dataTr, labelTr, dataTst, labelTst = self.splitTrainingTest(data,label,splitSizeTest=0.30,shuffle=True)
            print(data.shape)
            print(len(label))
            
            self.train_data.append(dataTr)
            self.train_label.append(labelTr)

            self.test_data.append(dataTst)
            self.test_label.append(labelTst)            

            N +=1
        print("----")

        
class Audio(object):
    def __init__(self, fileName):
        self.data = None
        self.samplerate = None        
        self.fileName = fileName
        self.category=None #Textual category
        self.label=None #Numerical label
        
    def read(self):
        """Reads the audio to get the raw data and sample rate"""
        self.data, self.samplerate = sf.read(self.fileName)
        return self.data,self.samplerate
    
    def showAudio(self):
        """Shows the audio in the jupyter notebook"""
        if(self.data.any()):
            return ipd.Audio(data=self.data, rate=self.samplerate)
        else:
            log.warning("Read file first")
            return None
        
    def plotAudio(self,title=None):
        """Plots the audio"""
        if(self.data.any()):
            plt.figure(figsize=(14, 5))
            librosa.display.waveplot(self.data, sr=self.samplerate)
            if(title is None):
                plt.title(self.fileName)
            else:
                plt.title(title)
        else:
            log.warning("Read file first") 
            
    def setCategory(self,cat):
        """Set the textual category"""
        self.category = cat
        
    def setLabel(self,lab):
        """Set the numerical label"""
        self.label = lab
        
    def MFCC_features(self,n_mfcc=20):
        """Generates the features of this audio and returns them."""
        if (self.audio.any()):
            mfcc_feat = librosa.feature.mfcc(y=self.audio, sr=self.samplerate, n_mfcc=20)
            mfcc_delta = librosa.feature.delta(mfcc_feat)
            features = np.hstack((mfcc_feat.T,mfcc_delta.T))
            return features            
        else:
            return None
        
    def MFCC(self,aud_d,rate,n_mfcc=20):
        """Generates the features of the given audio and returns it."""
        mfcc_feat = librosa.feature.mfcc(y=aud_d, sr=rate, n_mfcc=n_mfcc)
        mfcc_delta = librosa.feature.delta(mfcc_feat)

        features = np.hstack((mfcc_feat.T,mfcc_delta.T))

        return features
    
    def splitAudio(self,min_duration,deltaT):
        """Splits the file in chunks:
            min_duration: seconds to split the audio.
            deltaT: ##Missing a description from this##
        """
        
        #data,samplerate = sf.read(self.fileName) #we use the soundfile library instead of our own Audio.read to save memory by not storing it.
        if(not self.samplerate):
            self.read()#Read the data
            
        dataT, index = librosa.effects.trim(self.data)
        duration = len(dataT)*1./self.samplerate
        
        audio_data = None
        
        if duration >= min_duration: 
            #Split in n Chunks
            numChunks = int(duration/deltaT)
            sizeChunk = int(len(dataT)/numChunks)
            for lp in range(0,numChunks):    
                chunk = dataT[lp*sizeChunk:(lp+1)*sizeChunk]
                if lp != 0:
                    audio_data = np.vstack((audio_data, self.MFCC(chunk,self.samplerate))) # Using default n_mfcc=20
                else:
                    audio_data = self.MFCC(chunk,self.samplerate)
        if(audio_data is None):
            #Handle not splitable audios
            return self.MFCC(dataT,self.samplerate)
        else:
            return audio_data