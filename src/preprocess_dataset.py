#CLASS_NAMES
#This library helps to split the dataset using tensorflow mappers.
import pathlib
import tensorflow as tf
import numpy as np
#CLASS_NAMES
def getInfoDataset(fullPathDS):
    #Gets data_path, dataset size and class names.
    data_path = pathlib.Path(fullPathDS)
    image_count = len(list(data_path.glob('*/*.png')))
    DATASET_SIZE = image_count

    #Now get the classes:
    CLASS_NAMES = np.array([item.name for item in data_path.glob('*')])
    others = np.array(["train_audio", "test_audio", "valid_audio"])
    CLASS_NAMES = np.setdiff1d(CLASS_NAMES, others)

    return data_path,DATASET_SIZE,CLASS_NAMES
#Add a filter to only take the given classes and the given N samples per class:
def get_command(file_path,PLATFORM):
    if PLATFORM is "WINDOWS":
        parts = tf.strings.split(file_path, '\\')
    else:
        parts = tf.strings.split(file_path, '/')
    return parts[-2] #Return the second to last element, which is the label.

def filterCommands(file_path,PLATFORM,commands_t):
    command_name = get_command(file_path,PLATFORM)
    equal =  tf.math.equal(command_name, commands_t)
    # checking if all elements from  t1 match for all elements of some array in t2
    contains = tf.reduce_any(equal, axis=0)
    #contains = tf.reduce_any(equal_all)    
    return contains

def get_label(file_path,categories,PLATFORM):
  if PLATFORM is "WINDOWS":
    parts = tf.strings.split(file_path, '\\')
  else:
    parts = tf.strings.split(file_path, '/')
  return parts[-2] == categories

def decode_img(img,IMG_WIDTH,IMG_HEIGHT):
  img = tf.image.decode_png(img, channels=1) #color images
  img = tf.image.convert_image_dtype(img, tf.float32) 
   #convert unit8 tensor to floats in the [0,1]range
  return tf.image.resize(img, [IMG_WIDTH, IMG_HEIGHT]) 

def process_path(file_path,categories,PLATFORM,IMG_WIDTH,IMG_HEIGHT):
  label = get_label(file_path,categories,PLATFORM)
  img = tf.io.read_file(file_path)
  img = decode_img(img,IMG_WIDTH,IMG_HEIGHT)
  return img, label

def prepare_for_training(ds, train_per, val_per, test_per,DS_SIZE,BATCH_SIZE,AUTOTUNE, cache=True, shuffle_buffer_size=1000,epochs=10):
    train_size = int(train_per * DS_SIZE)
    val_size = int(val_per * DS_SIZE)
    test_size = int(test_per * DS_SIZE)


    if cache:
        if isinstance(cache, str):
            ds = ds.cache(cache)
        else:
            ds = ds.cache()
    #ds = ds.shuffle(buffer_size=shuffle_buffer_size)
    train_dataset = ds.take(train_size)
    test_dataset = ds.skip(train_size)
    val_dataset = test_dataset.skip(val_size)
    test_dataset = test_dataset.take(test_size)
    
    train_dataset = train_dataset.shuffle(buffer_size=shuffle_buffer_size,reshuffle_each_iteration=False)
    #train_dataset = train_dataset.repeat(epochs) #repeat forever  
    train_dataset = train_dataset.batch(BATCH_SIZE)  
    train_dataset = train_dataset.prefetch(buffer_size=AUTOTUNE)

    #test_dataset = test_dataset.repeat() #repeat forever  
    test_dataset = test_dataset.batch(BATCH_SIZE)  
    test_dataset = test_dataset.prefetch(buffer_size=AUTOTUNE)

    #val_dataset = val_dataset.repeat() #repeat forever  
    val_dataset = val_dataset.batch(BATCH_SIZE)  
    val_dataset = val_dataset.prefetch(buffer_size=AUTOTUNE)    

    """
    ds = ds.repeat() #repeat forever  
    ds = ds.batch(BATCH_SIZE)  
    ds = ds.prefetch(buffer_size=AUTOTUNE)
    """
    return train_dataset,test_dataset,val_dataset

#Preprocessing step
def getLabeledDS(data_path,DATASET_SIZE,CLASS_NAMES,AUTOTUNE,IMG_WIDTH,IMG_HEIGHT,commands="ALL",SAMPLES_PER_CLASS="ALL",PLATFORM="WINDOWS"):
    #Read all the files and get their path.
    list_ds = tf.data.Dataset.list_files(str(data_path/'*/*'))

    #for f in list_ds.take(5):
    #    print(f.numpy())
    
    final_dataset = None
    dsConfig= "" #Defines how to extraact the dataset.
    if(commands == "ALL"):
        #Use all commands
        if(SAMPLES_PER_CLASS=="ALL"):
            dsConfig = "allCommandsAllSamples"
        else:
            dsConfig = "allCommandsSomeSamples"
        commands = CLASS_NAMES.tolist()
    else:
        #Use only a subset of commands.
        if(SAMPLES_PER_CLASS=="ALL"):
            dsConfig = "someCommandsAllSamples"
        else:
            dsConfig = "someCommandsSomeSamples"    


        #commands contains the labels to use
        commands_t = tf.constant(commands,tf.string) # Classes we want as a tensor.

        if (dsConfig == "someCommandsAllSamples"):
            final_dataset = list_ds.filter(lambda x: filterCommands(x, PLATFORM,commands_t))

    # Filter per class and generate individual datasets per each class.
    if(dsConfig == "allCommandsSomeSamples" or dsConfig=="someCommandsSomeSamples"):
        list_datasets = []
        commands_t = tf.constant(commands,tf.string) # Classes we want
        for c in commands:
            commands_t = tf.constant([c],tf.string) # Classes we want
            list_ds_filtered = list_ds.filter(lambda x: filterCommands(x, PLATFORM,commands_t))
            list_datasets.append(list_ds_filtered.take(SAMPLES_PER_CLASS)) #Take only N from each.

    if(dsConfig == "allCommandsSomeSamples" or dsConfig=="someCommandsSomeSamples"):
        # Gather the data into a unique dataset.
        final_dataset = None
        for ds in list_datasets:
            if(final_dataset == None):
                final_dataset = ds
            else:
                final_dataset = final_dataset.concatenate(ds)

    if(dsConfig != "allCommandsAllSamples"):
        #Update important variables for training with the fresh dataset.
        print("Update DATASIZE variables")
        count = 0
        for x in final_dataset:
            count+=1
        DATASET_SIZE = count
        image_count = count #keep both variables updated, although they are the same.
        #Update CLASS_NAMES
        CLASS_NAMES = np.array(commands)
    print("There are ",DATASET_SIZE," samples in the filtered dataset.")

    #We will be using Dataset.map and num_parallel_calls is defined so that multiple images are loaded simultaneously.
    if(dsConfig == "allCommandsAllSamples"):
        #Use all samples per class.
        #labeled_ds = list_ds.map(process_path, num_parallel_calls=AUTOTUNE)
        labeled_ds = list_ds.map(lambda x: process_path(x, CLASS_NAMES,PLATFORM,IMG_WIDTH,IMG_HEIGHT), num_parallel_calls=AUTOTUNE)  
        
    else:
        #labeled_ds = final_dataset.map(process_path, num_parallel_calls=AUTOTUNE)
        labeled_ds = final_dataset.map(lambda x: process_path(x, CLASS_NAMES,PLATFORM,IMG_WIDTH,IMG_HEIGHT), num_parallel_calls=AUTOTUNE)  
    return labeled_ds, DATASET_SIZE,CLASS_NAMES