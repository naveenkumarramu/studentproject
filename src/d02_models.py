# Model Agnostic Meta Learning
# In this section are included the specific models used in our project.

"""Import Section """
import logging as log
from hmmlearn.hmm import GMMHMM
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

class Gmmhmm(object):
    """This model is an implementation of the Hidden Markov Model with Gaussin mixture emissions for audio classification."""
    def __init__(self,n_components=1,n_mix=2):
        self.n_components=n_components
        self.n_mix=n_mix
        self.models= [] #Models from GMMHM in a list
    def fit ( self , X , Y ) :

        self.models= [None] * len(Y) #Initialize list of size Y
        
        ##For each categorie train a new model.
        for cat in range(0,len(Y)):
            #Each sample in the training has a different length, this has to be sent to the trainer in the shape (length,)
            lengthTr = ((X[cat].shape[0], ))
            self.models[cat] = GMMHMM(n_components=self.n_components,n_mix=self.n_mix).fit(X[cat],lengths=lengthTr)
        
        return self.models

    def predict ( self , x ) :
        """Predict method."""
        #To predict a new sample, test all the stored Mixture Models and chose the one with max score.
        score_list=[]
        for model in self.models: 
            lengthTr = ((x.shape[0], ))
            score_list.append(model.score(x))

        return np.argmax(score_list)
    def predictClass(self, x,categoriesL):
        """ Predicts the class and returns the category from the given list."""
        return categoriesL[self.predict(x)]
    def score ( self , X, Y) :
        """Score method."""
        final_test = []

        for cat in range(0,len(Y)):
            for sample in X[cat]:
                final_test.append(self.predict(sample))
        #To use the Y vector of labels which is divided into each category, it is necessary to first stack all the labels.
        stacked_labels = np.hstack(Y)

        s = round(accuracy_score(stacked_labels, final_test)*100,2)
        conf_mat = confusion_matrix(stacked_labels, final_test)

        return s,conf_mat
    def printConfusionMatrix(self,conf_mat,categories, acc,header="GMMHMM Model"):
        """Displays a colored plot of the confusion matrix"""
        cm_df = pd.DataFrame(conf_mat,
                            index = categories, 
                            columns = categories)
        plt.figure(figsize=(15,10))
        sns.heatmap(cm_df, annot=False)
        plt.title(header+"\nAccuracy: "+str(round(acc,2)))
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.show()        
#This is a template, copy paste everything to define a new model.

class FCN(nn.Module):
    """Model description"""
    def __init__(self,num_cats):

        self.name = 'FCN'

        super(FCN, self).__init__()
        self.conv1 = nn.Conv2d(1, 128, kernel_size=(8,8))
        self.conv1_bn = nn.BatchNorm2d(128)
        self.conv2 = nn.Conv2d(128, 256, kernel_size=(5,5))
        self.conv2_bn = nn.BatchNorm2d(256)
        self.conv3 = nn.Conv2d(256, 128, kernel_size=(3,3))
        self.conv3_bn = nn.BatchNorm2d(128)
        self.globalAvgPool =  nn.AvgPool2d(27, 87)
        self.fc1 = nn.Linear(128, num_cats)
        
    def forward(self, x):
        x = F.relu(self.conv1_bn(self.conv1(x)))
        x = F.relu(self.conv2_bn(self.conv2(x)))
        x = F.relu(self.conv3_bn(self.conv3(x)))
        x = self.globalAvgPool(x)
        x = x.view(x.size(0),-1)
        x = self.fc1(x)
        return x
    
class CNN(nn.Module):
    #This architecture comes from reference [2]
    def __init__(self,num_cats):

        self.name = 'CNN'

        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(1, 50, kernel_size=(8,8))
        self.conv1_bn = nn.BatchNorm2d(50)

        self.conv2 = nn.Conv2d(50, 100, kernel_size=(5,5))
        self.conv2_bn = nn.BatchNorm2d(100)

        self.conv3 = nn.Conv2d(100, 50, kernel_size=(3,3))
        self.conv3_bn = nn.BatchNorm2d(50)

        self.fc1 = nn.Linear(900, 400)
        self.drop1 = nn.Dropout()
        self.fc2 = nn.Linear(400, 100)
        self.drop2 = nn.Dropout()
        self.fc3 = nn.Linear(100, num_cats)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1_bn(self.conv1(x)), 2))
        x = F.relu(F.max_pool2d(self.conv2_bn(self.conv2(x)), 2))
        x = F.relu(F.max_pool2d(self.conv3_bn(self.conv3(x)), 2))
        #x = x.view(-1, x.size(0))
        x = x.view(x.size(0),-1)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x
        
#This is a template, copy paste everything to define a new model.
class NewModel(object):
    """Model description"""
    def __init__(self):       
        self.example=None
        print("Initial variables")
        
    def fit ( self , X , Y ) :
        """Fit method."""
        return self
    def predict ( self , X ) :
        """Predict method."""
        return "prediction label"

    def score ( self , X, Y) :
        """Score method."""
        return "score of the prediction"

    def info(self):
        log.info("This model does nothing yet")